###
# Copyright (c) 2014, Zaptan Kitsune
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

###

import supybot.utils as utils
from supybot.commands import *
import supybot.plugins as plugins
import supybot.ircutils as ircutils
import supybot.callbacks as callbacks
import random, re


class Roll(callbacks.PluginRegexp):
    """Roll a Dice, Try your luck!"""
    """unaddressedRegexps = ('rollSnarfer')"""

    def _zeParse(self, expr, symbols={}):
        result = re.sub('\(([0-9]*[d][0-9]+)\)',self._rerun,expr)
        result = re.sub('([0-9]*[d][0-9]+)',self._rerun,result)
        res1 = re.sub('\]\[','+',result)
        res1 =re.sub('\[|\]','',res1)
        ans = self._safe_eval(res1)
        return "%s = %d" % (result,ans)

    def _safe_eval(self, expr, symbols={}):
        return eval(expr, dict(__builtins__=None), symbols)

    def roll(self, irc, msg, args, expr):
        irc.reply(self._zeParse(expr))
    roll = wrap(roll, ['text'])

    def troll(self, irc, msg, args, expr):
        result = re.sub('\(([0-9]*[d][0-9]+)\)',self._rerun,expr)
        result = re.sub('([0-9]*[d][0-9]+)',self._rerun,result)
        res1 = re.sub('\]\[','+',result)
        res1 =re.sub('\[|\]','',res1)
        ans = self._safe_eval(res1)
        irc.reply("%d" % (ans))
    troll = wrap(troll, ['text'])

    def _rerun(self, mobj):
        r = mobj.group(1).split("d")
        if r[0] == "":
            r[0] = 1
        d = []
        for i in range(int(r[0])):
            d.append(random.randint(1, int(r[1])))
        return "([%s])" % (']['.join(map(str, d)))

    def rollSnarfer(self, irc, msg, match):
        channel = msg.args[0]
        if not irc.isChannel(channel):
            return
        if callbacks.addressed(irc.nick, msg):
            return
        irc.reply(self._zeParse(msg))
    rollSnarfer.__doc__ = r'([0-9]*[d][0-9]+)'
    

Class = Roll


# vim:set shiftwidth=4 softtabstop=4 expandtab textwidth=79:


