###
# Copyright (c) 2014, Zaptan Kitsune
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

###

import supybot.utils as utils
from supybot.commands import *
import supybot.plugins as plugins
import supybot.ircutils as ircutils
import supybot.callbacks as callbacks
import supybot.world as world
import supybot.log as log
import supybot.conf as conf
import pickle, time, re, sys
from dateutil import parser

TIME = time

events = []
datadir = conf.supybot.directories.data()
filename = conf.supybot.directories.data.dirize('conlist.txt')
log.error(filename)

def parse(s):
    todo = []
    s = s.replace('noon', '12:00')
    s = s.replace('midnight', '00:00')
    if 'tomorrow' in s:
        todo.append(lambda i: i + 86400)
        s = s.replace('tomorrow', '')
    if 'next week' in s:
        todo.append(lambda i: i + 86400*7)
        s = s.replace('next week', '')
    i = int(time.mktime(parser.parse(s, fuzzy=True).timetuple()))
    for f in todo:
        i = f(i)
    return i


class TTC(callbacks.Plugin):
    """ttc -- time till con"""
    def __init__(self, irc):
        self.__parent = super(TTC, self)
        self.__parent.__init__(irc)
        self._load()

    def _load(self):
        self.log.debug(datadir)
        self.log.debug(filename)
        try:
            pkl = open(filename, "rb" )
            try:
                events = pickle.load( pkl )
                events.sort()
            except Exception, e:
                self.log.error('Can not load Data: %s', e)
            pkl.close()
        except IOError, e:
            self.log.error('Can not find File: %s', e)


    def die(self):
        self._save()
        self.__parent.die()

    def _save(self):
        try:
             pkl = open(filename, 'wb')
             try:
                 pickle.dump(events, pkl)
             except Exception, e:
                 self.log.warning('Unable to store pickled data: %s', e)
             pkl.close()
        except (IOError), e:
             self.log.warning('File error: %s', e)



    def register(self, irc, msg, args, d):
        """(<name> <date>) -- This will add the con to the list on the date listed, this will update the date if the name already exists. date must be in format: YYYY-MM-DD-hh:mm"""
        e = d.split() 
        events.append((e[0],e[1]))
        irc.replySuccess()
        events.sort()
        self._save()
    register = wrap(register, ['text'])

    def ttc(self, irc, msg, args, name):
        """(<name>|list) -- Shows the time till the con listed under <name>. List will show the <names> added to the list. """
        if events:
            self._load()
        if (name.lower() == "list"):
            stmsg = ""
            for evt in events:
                stmsg += "(%s: %s) " % (evt[0], evt[1])
            irc.reply(stmsg)
        else:
            for e in events:
                if (name.lower() == e[0].lower()):
                    now = int(time.time())
                    new = parse(e[1])
                    if new != now:
                        if new - now < 0:
                            new += 86400
                        str = " till %s" % (e[0])
                        irc.reply(utils.timeElapsed(new-now) + str)
                    else:
                        irc.error('That\'s right now!')
    ttc = wrap(ttc, ['text'])

Class = TTC


# vim:set shiftwidth=4 softtabstop=4 expandtab textwidth=79:
